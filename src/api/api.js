import axios from 'axios';

const API = axios.create({ baseURL: 'http://localhost:3001' });

//Admin routes and endpoints
export const checkAllRooms = () => API.get('/admin/adminReadRooms');

export const checkRoomsByCity = () => API.get('/admin/adminReadRoomsByCity');

export const getRoomById = (id) => API.get(`/admin/adminGetRoomById/${id}`);

export const addRoom = (room) => API.post('/admin/adminAddRoom', room);

export const updateRoom = (id, room) => API.put(`/admin/adminUpdateRoom/${id}`, room);

export const removeRoom = (id) => API.delete(`/admin/adminDeleteRoom/${id}`);

//Routes and endpoints to handle all information related to users
export const CheckIfUserExists = async (email) => API.post('/users/check', { email: email });

export const LoginUser = async (email) => API.post('/users/login', { email: email });

export const NewUser = async () => API.post('/users/create');
