import Navbar from './components/Navbar';
import { AuthNavigation } from './Navigator/AuthNavigation';
import { Layout } from './pages/LayOut/Layout';

function App() {
	return (
		<Layout>
			<Navbar />
			<AuthNavigation />
		</Layout>
	);
}

export default App;
