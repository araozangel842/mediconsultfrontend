import React, { useState, useEffect } from 'react';
import Login from '../pages/Login';

import { Inside } from '../Navigator/Navigation';

export const AuthNavigation = () => {
	const [authUser, setAuthUser] = useState('');

	useEffect(() => {
		setAuthUser(localStorage.getItem('user'));
	}, []);

	return <>{authUser ? <Inside /> : <Login />}</>;
};
