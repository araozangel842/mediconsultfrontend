import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Admin from '../pages/Administrator/Admin';
import Home from '../pages/Home/Home';

import Login from '../pages/Login';
import { RoomById } from '../pages/RoomId/RoomById';
import Reservation from '../pages/User/Reservation';

export const Inside = () => {
	return (
		<Routes>
			<Route path="/" element={<Home />} />
			<Route path="/home" element={<Home />} />

			{/* All routes for Admin in one place */}
			<Route path="/admin" element={<Admin />} />
			<Route path="/adminGetRoomById/:id" element={<RoomById />} />

			{/* Reservation place for user */}
			<Route path="/reservation" element={<Reservation />} />
			<Route path="/login" element={<Login />} />
		</Routes>
	);
};
