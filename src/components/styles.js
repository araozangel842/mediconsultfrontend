import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	navbar: {
		width: '96%',
		height: '50px',
		margin: 'auto',
		padding: '10px',
		display: 'flex',
	},
	insideNavbar: {
		width: '100%',
		display: 'flex',
		backgroundColor: 'rgba(255,255,255,0.6)',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderRadius: '5px',
		boxShadow: '0 0 15px rgba(255,255,255,.695)',
		color: 'rgb(0,91,126)',
	},
	left: {
		display: 'flex',
		alignItems: 'center',
		marginLeft: '10px',
	},
	center: {
		display: 'flex',
		alignItems: 'center',
	},
	right: {
		display: 'flex',
		alignItems: 'center',
		marginRight: '10px',
	},
	//backButton
	backButton: {
		position: 'fixed',
		right: 20,
		bottom: 20,
		backgroundColor: 'rgb(40,150,194)',
		padding: 8,
		borderRadius: '50%',
		cursor: 'pointer',
		boxShadow: '0 0 10px rgba(0,0,0,0.2)',
		transition: 'padding .32s, right .2s, bottom .1s',
		'&:hover': {
			backgroundColor: 'rgb(0,91,126)',
			boxShadow: '0 0 19px rgba(0,0,0,0.3)',
			padding: 10,
			right: 26,
			bottom: 22,
		},
	},
	link: {
		position: 'relative',
		width: '100%',
		height: '100%',
		display: 'flex',
		margin: '0 auto',
		color: 'rgba(255,255,255,0.94)',
		'&:hover': {
			color: 'rgba(255,255,255,0.84)',
		},
	},
});
