export const InputField = ({ type = 'text', name, className = '', ...props }) => (
	<>
		<label className={`inputField ${className}`}>
			<h2>{name}</h2>
			<input className={`input ${className}`} {...props}></input>
		</label>
	</>
);
