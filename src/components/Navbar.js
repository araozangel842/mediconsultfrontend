import React from 'react';
import { useStyles } from './styles';
import PermPhoneMsgIcon from '@mui/icons-material/PermPhoneMsg';
import Key from '@mui/icons-material/Key';
import { Facebook, Instagram, Language, LinkedIn, Twitter } from '@mui/icons-material';

const Navbar = () => {
	const clases = useStyles();
	return (
		<>
			<div className={clases.navbar}>
				<div className={clases.insideNavbar}>
					<div className={clases.left}>
						<div style={{ marginRight: 5 }}>
							<PermPhoneMsgIcon />
						</div>
						<h4>Call us: </h4>
						<small>+358 10 524 5500</small>
					</div>
					<div className={clases.center}>
						<Key />
						Log to the customer support
					</div>
					<div className={clases.right}>
						<Language />
						<div style={{ display: 'flex', alignItems: 'center' }}>
							Follow us:
							<Facebook />
							<Twitter />
							<LinkedIn />
							<Instagram />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Navbar;
