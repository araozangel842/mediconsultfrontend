import { Home } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import { useStyles } from './styles';

export const GoBack = () => {
	const classes = useStyles();
	return (
		<div className={classes.backButton}>
			<Link to="/home">
				<div className={classes.link}>
					<Home />
				</div>
			</Link>
		</div>
	);
};
