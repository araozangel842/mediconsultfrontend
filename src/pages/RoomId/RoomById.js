import { Button } from '@mui/material';
import React, { useState } from 'react';
import { removeRoom, updateRoom } from '../../api/api';
import { InputField } from '../../components/Input';
import { useStyles } from './styles';

export const RoomById = ({ oneRoom }) => {
	const { _id, adminId, city, room, parking, chairsAmount, chairType1, chairType2, chairType3 } =
		oneRoom;
	console.log('--------' + oneRoom);
	console.log('--------' + adminId);
	const [roomId, setRoomId] = useState({
		adminId: adminId,
		city: city,
		room: room,
		parking: parking,
		chairsAmount: chairsAmount,
		chairType1: chairType1,
		chairType2: chairType2,
		chairType3: chairType3,
		date: Date.now(),
	});

	const classes = useStyles();
	const [isOpen, setIsOpen] = useState(false);
	const [selectedOption, setSelectedOption] = useState(city);
	const options = ['Turku', 'Helsinki', 'Oulu', 'Salo', 'Joensu'];

	const setOption = (value) => {
		setSelectedOption(value);
		setRoomId({ ...roomId, city: value });
		setIsOpen(false);
	};
	const sumatoria =
		Number(roomId.chairType1) + Number(roomId.chairType2) + Number(roomId.chairType3);
	console.log(oneRoom);

	const updateThisRoom = () => {
		updateRoom(_id, roomId).then((r) => console.log('Room updated: ' + r));
		window.location.replace('/admin');
		//closeEditAndDelete(false);
	};

	const removeThisRoom = () => {
		removeRoom(_id).then((room) => console.log('Room removed'));
		//closeEditAndDelete(false);
		window.location.replace('/admin');
	};

	return (
		<>
			<div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
				<div
					style={{
						width: '80vh',
						height: '100%',
						backgroundColor: 'rgba(255, 255, 255, 0.09831)',
						paddingTop: '3px ',
						paddingBottom: '10px',
						marginTop: '10px',
						borderRadius: '5px',
						display: 'flex',
						justifyContent: 'space-around',
					}}
				>
					<div className={classes.left}>
						<div>
							<InputField
								name="City"
								value={roomId.city}
								className={[classes.inputField, classes.input]}
								onChange={(event) => setRoomId({ ...roomId, city: event.target.value })}
								required
							/>
						</div>
						<div className={classes.mainDropdown}>
							<div className={classes.dropDownHeader} onClick={() => setIsOpen(!isOpen)}>
								{selectedOption}
							</div>

							{isOpen && (
								<ul className={classes.dropDownList}>
									{options.map((option, index) => (
										<li
											className={classes.listItem}
											key={index}
											onClick={(e) => setOption(e.target.innerText)}
										>
											{option}
										</li>
									))}
								</ul>
							)}
						</div>
					</div>
					<div className={classes.right}>
						<div>
							<InputField
								name="Room number"
								value={roomId.room}
								onChange={(event) => setRoomId({ ...roomId, roomId: event.target.value })}
								required
							/>
						</div>
						<div>
							Parking
							<label>
								<input
									type="radio"
									name="Parking"
									value={roomId.parking}
									onChange={(event) => setRoomId({ ...roomId, parking: false })}
								/>
								No
							</label>
							<label>
								<input
									type="radio"
									name="Parking"
									value={roomId.parking}
									onChange={(event) => setRoomId({ ...roomId, parking: true })}
								/>
								Si
							</label>
						</div>

						<div>
							<input
								type="number"
								id="number"
								min={4}
								max={30}
								placeholder={chairsAmount}
								name="Amount of chairs"
								onClick={(event) => setRoomId({ ...roomId, chairsAmount: event.target.value })}
							/>
						</div>
						<div>
							<h2>With 2 monitors: {roomId.chairType1}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder={chairType1}
								name="chairType1"
								onClick={(event) => setRoomId({ ...roomId, chairType1: event.target.value })}
							/>
						</div>
						<div>
							<h2>With 3 monitors: {roomId.chairType2}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder={chairType2}
								name="chairType2"
								onClick={(event) => setRoomId({ ...roomId, chairType2: event.target.value })}
							/>
						</div>

						<div>
							<h2>With 4 monitors: {roomId.chairType3}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder={chairType3}
								name="chairType3"
								onClick={(event) => setRoomId({ ...roomId, chairType3: event.target.value })}
							/>
						</div>
					</div>
					{/* <h2> Anithing else:</h2> */}
				</div>

				<div className={classes.divButtons}>
					<Button variant="contained" color="error" onClick={() => removeThisRoom(_id)}>
						Delete this room
					</Button>
					{sumatoria > roomId.chairsAmount ? (
						<Button variant="contained" disabled>
							<span
								style={{
									color: '#fe5f75',
									textShadow: '0 .48px 5px 1px rgba(255, 0, 0, 0.1)',
									fontWeight: 'bolder',
								}}
							>
								Total of chairs can't be less than the amount of different types ofchairs
							</span>
						</Button>
					) : (
						<Button variant="contained" color="success" onClick={() => updateThisRoom(_id, roomId)}>
							{sumatoria === roomId.chairsAmount
								? 'Seems like the room has the same amount of chairs'
								: 'Update.'}
						</Button>
					)}
				</div>
			</div>
		</>
	);
};
