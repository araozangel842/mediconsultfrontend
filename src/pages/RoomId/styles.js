import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	//Dropdown
	mainDropdown: {
		width: '10.5rem',
		margin: '0 auto',
		postion: 'relative',
	},
	dropDownHeader: {
		marginBottom: '.8rem',
		marginTop: '.8rem',
		padding: '.4em 2em .4em 1em',
		boxShadow: '2px 5px 13px rgba(0, 0, 0, 0.16)',
		fontWeight: 'bold',
		fontSize: '1.3rem',
		color: '#3faffa',
		backgroundColor: '#ffffff',
		borderRadius: '5px',
		textAlign: 'center',
		cursor: 'pointer',
		postion: 'absolute',
		zIndex: 1000000,
		'&:hover': {
			backgroundColor: 'rgba(0,0,0,0.2)',
			color: 'white',
		},
	},
	dropDownList: {
		padding: 0,
		margin: 0,
		boxShadow: '2px 5px 13px rgba(0, 0, 0, 0.16)',
		backgroundColor: '#fff',
		border: '1px solid #e5e5e5',
		boxSizing: 'border-box',
		color: '#3faffa',
		fontSize: '1.3rem',
		fontWeight: 'bold',
		borderRadius: '5px',
	},
	listItem: {
		listStyle: 'none',
		marginBottom: '.8rem',
		margin: 'auto',
		textAlign: 'center',
		cursor: 'pointer',
		'&:hover': {
			backgroundColor: '#e5e5e5',
			fontWeight: 'bolder',
			border: '1px solid #e5e5e5',
			color: 'rgba(0,0,0,0.68)',
		},
	},
	image: {
		width: '100px',
		height: '100px',
	},
	divButtons: {
		width: '80%',
		display: 'flex',
		justifyContent: 'space-around',
		margin: '.98rem auto',
	},
});
