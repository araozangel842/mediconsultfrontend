import { Button, TextField } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LoginUser } from '../api/api';

import { useStyles } from './styles';

const Login = () => {
	const navigate = useNavigate();
	const clases = useStyles();

	const [email, setEmail] = useState('');
	const [user, setUser] = useState('');

	const getUserAndLogin = async (e) => {
		//e.preventDefault();
		await LoginUser(email)
			.then((response) => {
				setUser(response.data);
			})
			.catch((err) => console.log(err));

		if (user) {
			JSON.stringify(localStorage.setItem('user', user.userToken));
			JSON.stringify(localStorage.setItem('userMail', email));
			window.location.replace('/home');
		}
		// const check = await CheckIfUserExists(email);
		// console.log(check.data.user);
		// JSON.stringify(localStorage.setItem('userData', check.data.user.userMail));
	};
	const go = () => {
		navigate('/home');
	};

	return (
		<div className={clases.login}>
			<h1>Login Form</h1>
			<TextField
				name="email"
				variant="outlined"
				label="Write your email to sign in"
				value={email}
				onChange={(e) => setEmail(e.target.value)}
			/>
			<Button onClick={getUserAndLogin} variant="contained" disabled={!email}>
				{email ? 'Go Inside' : 'Write your email'}
			</Button>
			<Button onClick={go}>Go in </Button>
		</div>
	);
};

export default Login;
