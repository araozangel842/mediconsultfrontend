import { Button } from '@mui/material';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Profile } from './Profile/Profile';

import { useStyles } from './styles';

const Home = () => {
	const [open, setOpen] = useState(false);

	const classes = useStyles();

	return (
		<div className={classes.main}>
			<h1>Home</h1>
			<div className={classes.buttons}>
				<Link
					style={{
						textDecoration: 'none',
						backgroundColor: '#553',
						padding: '0.5em .64rem',
						borderRadius: '5px',
						color: '#ffffff',
						boxShadow: '0 0 5px rgba(2,0,0,.695)',
					}}
					to="/admin"
				>
					Admin place
				</Link>

				<Link
					style={{
						textDecoration: 'none',
						backgroundColor: '#553543',
						fontWeight: 'bold',
						padding: '0.5em .64rem',
						borderRadius: '5px',
						color: '#ffffff',
						boxShadow: '0 0 5px rgba(2,0,0,.695)',
					}}
					to="/reservation"
				>
					User
				</Link>

				<Button
					variant="contained"
					color={open ? 'error' : 'primary'}
					onClick={() => setOpen(!open)}
				>
					{!open ? 'Dashboard' : 'Close'}
				</Button>
			</div>

			{open && <Profile />}
		</div>
	);
};

export default Home;
