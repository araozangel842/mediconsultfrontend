import React from 'react';
import { useStyles } from '../../styles';

export const History = () => {
	const classes = useStyles();

	return (
		<div className={classes.history}>
			<h1>Reservation history</h1>
		</div>
	);
};
