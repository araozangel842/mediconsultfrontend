import React, { useEffect, useState } from 'react';
import { CheckIfUserExists } from '../../../api/api';
import { GoBack } from '../../../components/GoToHome';
import { useStyles } from '../styles';
import { History } from './History/History';

export const Profile = () => {
	const classes = useStyles();
	const [userData, setUserData] = useState();
	const [userHistory, setUserHistory] = useState(false);
	useEffect(() => {
		CheckIfUserExists(localStorage.getItem('userMail')).then((response) =>
			setUserData(response.data.user)
		);
	}, []);
	const { displayName, defaultSearch, userMail } = userData !== undefined && userData[0];

	const historyDataUser = () => {
		setUserHistory(!userHistory);
	};

	return (
		<div className={classes.profile}>
			<div className={classes.mainProfile}>
				<div className={classes.left}>
					<div className={classes.image}>Upload image</div>
					<div className={classes.content}>
						<h2>{displayName}</h2>
						<h3 className={classes.hLeftContent}>
							Role: <span className={classes.spanProfile}>Software Engineer</span>
						</h3>
						<h3 className={classes.hLeftContent}>
							Email: <span className={classes.spanProfile}>{userMail}</span>
						</h3>
						<h3 className={classes.hLeftContent}>
							City: <span className={classes.spanProfile}>{defaultSearch}</span>
						</h3>
					</div>
				</div>
				<div className={classes.right}>
					<h1>Upcoming reservation details</h1>
					<h3 className={classes.hRightContent}>
						City: <span className={classes.spanDetails}>Turku</span>
					</h3>

					<h3 className={classes.hRightContent}>
						Building: <span className={classes.spanDetails}>Main building</span>
					</h3>
					<h3 className={classes.hRightContent}>
						Room: <span className={classes.spanDetails}>Romeo</span>
					</h3>
					<h3 className={classes.hRightContent}>
						Spot: <span className={classes.spanDetails}>Flex spot</span>
					</h3>
					<h3 className={classes.hRightContent}>
						Date: <span className={classes.spanDetails}> 10/10/22, Monday</span>
					</h3>

					<div
						className={!userHistory ? classes.historyContent : classes.historyContentClose}
						onClick={historyDataUser}
					>
						{!userHistory ? 'History' : 'Close'}
					</div>
				</div>
			</div>
			{userHistory && <History />}

			<GoBack />
		</div>
	);
};
