import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	main: {
		position: 'relative',
		display: 'flex',
		flexDirection: 'column',
	},
	buttons: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-around',
	},
	//Profile component styles
	profile: {
		display: 'flex',
		margin: '0 auto',
		position: 'relative',
	},
	mainProfile: {
		position: 'relative',
		width: '90vw',
		height: '100%',
		display: 'flex',
		textAlign: 'center',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: 'rgba(0,0,0,0.0567292)',
		borderRadius: '7px',
		boxShadow: '0 0 10px rgba(0,0,0,0.21215)',
		paddingTop: '100px',
		paddingBottom: '100px',
		marginTop: '70px',
	},
	left: {
		height: 'fit-content',
		padding: '20px',
	},
	content: {
		textAlign: 'left',
	},
	image: {
		width: '100px',
		height: '100px',
		backgroundColor: 'silver',
		borderRadius: '50%',
		display: 'flex',
		alignItems: 'center',
		textAlign: 'center',
	},
	hLeftContent: {
		display: 'flex',
		flexDirection: 'column',
		color: 'rgba(0,0,0,0.895)',
	},
	spanProfile: {
		width: '100%',
		paddingBottom: '2px',
		borderBottom: '1px solid rgba(63,175,250,.5)',
		marginTop: '20px',
		fontSize: '16px',
		color: 'rgba(0,0,0,0.55)',
	},
	right: {
		border: '1px solid rgba(0,0,0,0.107)',
		textAlign: 'left',
		padding: '25px 55px',
		position: 'relative',
		boxShadow: '0 0 10px rgba(0,0,0,0.123)',
		borderRadius: '6px',
	},
	hRightContent: {
		display: 'flex',
		flexDirection: 'column',
		color: 'rgba(0,0,0,0.925)',
	},
	spanDetails: {
		width: 'fit-content',
		color: 'rgba(0,0,0,0.645)',
		paddingTop: '10px',
		paddingLeft: '1px',
		fontSize: '16px',
		borderBottom: '1px solid rgba(63,175,250,.5)',
	},
	historyContent: {
		position: 'absolute',
		bottom: 5,
		right: 35,
		backgroundColor: 'rgba(22,106,217,.589)',
		width: 'fit-content',
		padding: '10px 20px',
		borderRadius: '5px',
		fontWeight: 'bold',
		color: 'white',
		'&:hover': {
			backgroundColor: 'rgba(22,106,217,.789)',
			cursor: 'pointer',
			color: 'rgba(237,240,242,.93)',
			border: '1px solid rgba(0,0,0,0.013)',
			boxShadow: '0 0 5px rgba(0,0,0,0.3)',
		},
	},
	historyContentClose: {
		position: 'absolute',
		bottom: 5,
		right: 35,
		backgroundColor: 'crimson',
		width: 'fit-content',
		padding: '10px 20px',
		borderRadius: '5px',
		fontWeight: 'bold',
		color: 'white',
		cursor: 'pointer',
	},
	//UserHistory component styles
	history: {
		position: 'absolute',
		width: '60%',
		height: '70%',
		top: '120px',
		left: '80px',
		backgroundColor: 'rgba(255,255, 255, 0.41)',
		backdropFilter: 'blur(6px)',
		boxShadow: '0 0 20px rgba(0,0,0,0.2)',
		borderRadius: '7px',
		textAlign: 'center',
		'&:hover': {
			backdropFilter: 'blur(7px)',
			boxShadow: '0 0 25px rgba(0,0,0,.29)',
			cursor: 'pointer',
		},
	},
});
