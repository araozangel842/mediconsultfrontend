import {
	Accessible,
	Cancel,
	Create,
	DoNotDisturbAltTwoTone,
	Keyboard,
	Laptop,
	Monitor,
	Videocam,
} from '@mui/icons-material';
import React, { useEffect, useState } from 'react';
import { useStyles } from './styles';
// import { uuid } from 'uuidv4';
import { v4 as uuidv4 } from 'uuid';

const SendRoom = () => {
	const classes = useStyles();
	const [oneChair, setOneChair] = useState([]);
	const [valueChair, setValueChair] = useState([]);
	const [allChairs, setAllChairs] = useState([]);
	console.log('valueChair', valueChair);
	console.log('allChairs', allChairs);
	console.log('oneChair', oneChair);
	// console.log(uuidv4());

	const handleUpdate = (listItem) => {
		const updatedChairs = allChairs.map((chair) => {
			if (chair.id === listItem.id) {
				// ...do the update here
				return {
					...chair,
					isActivated: true,
				};
			}
			return chair;
		});
		setAllChairs(updatedChairs);
	};
	return (
		<div>
			<button
				style={{
					width: '100px',
					height: '30px',
					padding: '5px',
					backgroundColor: 'red',
					color: 'white',
				}}
				onClick={() => {
					const oneNewChair = [...oneChair];
					oneNewChair.push(<OneChair setValueChair={setValueChair} />);
					//oneNewChair.push(<OneChair setValueChair={setOneChair} />);
					setOneChair(oneNewChair);
				}}
			>
				More
			</button>
			<button
				style={{
					width: '100px',
					height: '30px',
					padding: '5px',
					backgroundColor: 'green',
					color: 'black',
				}}
				onClick={() => {
					const deleteChair = [...oneChair];
					// deleteChair.splice(deleteChair.length - 1, 1);
					deleteChair.pop();
					setOneChair(deleteChair);
				}}
			>
				Less
			</button>
			<div className={classes.onChairHeader}>
				<span className={classes.td}>Spot</span>
				<span className={classes.td}>Status</span>
				<span className={classes.td}>
					<Monitor />
				</span>
				<span className={classes.td}>
					<Accessible />
				</span>
				<span className={classes.td}>
					<Videocam />
				</span>
				<span className={classes.td}>
					<Laptop />
				</span>
				<span className={classes.td}>
					<Keyboard />
				</span>
				<span className={classes.tdNote}>
					Notes
					<Create />
				</span>
				{/* </div> */}
				{/* <OneChair setValueChair={setValueChair} /> */}
			</div>

			<div>
				{oneChair.map((component, index) => (
					<div
						style={{ display: 'flex', justifyContent: 'space-between', marginTop: '10px' }}
						key={index}
					>
						<p>{index + 1}</p>
						{component}
						{/* <span>{component.id}</span>
						<span> {component.isActive ? ' active' : ' disabled'}</span>
						<span>{component.otherScreens ? 'yes' : 'no'}</span>
						<span>{component.chair ? 'yes' : 'no'}</span>
						<span>{component.camera ? 'yes' : 'no'}</span>
						<span>{component.laptop ? 'yes' : 'no'}</span>
						<span>{component.keyboard ? 'yes' : 'no'}</span>
						<span>{component.note ? 'yes' : 'no'}</span> */}
					</div>
				))}
			</div>
			<div
				style={{ width: '100px', height: '20px', backgroundColor: 'black', color: 'whitesmoke' }}
				onClick={() => console.log(oneChair)}
			>
				Console
			</div>
		</div>
		// </div>
	);
};

export default SendRoom;

const OneChair = ({ setValueChair }) => {
	const classes = useStyles();
	const chairId = uuidv4();
	const [oneChair, setOneChair] = useState({
		id: chairId,
		userName: '',
		userEmail: '',
		isActive: false,
		otherScreens: false,
		chair: true,
		camera: true,
		laptop: true,
		keyboard: true,
		notes: '',
	});
	useEffect(() => {
		console.log('ADDING NEW CHARI');
		setValueChair((prevState) => [...prevState, oneChair]);
		//setValueChair(oneChair);
	}, []);

	return (
		<>
			<div>
				<div className={classes.onChair}>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.isActive}
							onChange={(event) => setOneChair({ ...oneChair, isActive: !oneChair.isActive })}
						/>
						<span className={classes.thActiveHead}>
							{!oneChair.isActive && <DoNotDisturbAltTwoTone color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.otherScreens}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									otherScreens: !oneChair.otherScreens,
								})
							}
						/>
						<span className={classes.thActiveOtherHead}>
							{!oneChair.otherScreens && <Cancel color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.chair}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									chair: !oneChair.chair,
								})
							}
						/>
						<span className={classes.thActiveOtherHead}>
							{!oneChair.chair && <Cancel color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.camera}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									camera: !oneChair.camera,
								})
							}
						/>
						<span className={classes.thActiveOtherHead}>
							{!oneChair.camera && <Cancel color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.laptop}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									laptop: !oneChair.laptop,
								})
							}
						/>
						<span className={classes.thActiveOtherHead}>
							{!oneChair.laptop && <Cancel color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<input
							type="checkbox"
							value={oneChair.keyboard}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									keyboard: !oneChair.keyboard,
								})
							}
						/>
						<span className={classes.thActiveOtherHead}>
							{!oneChair.keyboard && <Cancel color="error" />}
						</span>
					</span>
					<span className={classes.thHead}>
						<textarea
							style={{ width: '96%' }}
							type="text"
							value={oneChair.notes}
							onChange={(event) =>
								setOneChair({
									...oneChair,
									notes: event.target.value,
								})
							}
						/>
					</span>
				</div>
			</div>
			{/* <div
				style={{
					width: '200px',
					height: '20px',
					backgroundColor: 'blue',
					color: 'white',
				}}
				onClick={(event) => setValueChair(oneChair)}
			>
				Click all values
			</div> */}
		</>
	);
};
