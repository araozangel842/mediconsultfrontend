import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	//One chair component styles
	onChairHeader: {
		width: '90vw',
		display: 'flex',
		backgroundColor: '#f5f5f5',
		justifyContent: 'space-between',
		borderRadius: '5px',
	},
	onChair: {
		width: '90vw',
		display: 'flex',
	},
	td: {
		position: 'relative',
		padding: '.56em .45em',
		cursor: 'pointer',
	},
	tdNote: {
		display: 'flex',
		alignItems: 'center',
		padding: '.6em .5em',
		justifyContent: 'center',
	},
	thActiveHead: {
		position: 'absolute',
		top: -1,
		right: -28,
	},
	thActiveOtherHead: {
		position: 'absolute',
		top: -1,
		right: -30,
	},
	thHead: {
		position: 'relative',
		padding: 'o.5em',
		textAlign: 'center',
		margin: '0 auto',
		'&:hover': {
			cursor: 'pointer',
		},
	},
});
