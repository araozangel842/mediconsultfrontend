import { Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { addRoom, checkAllRooms, getRoomById } from '../../api/api';
import { useStyles } from '../styles';
import {
	Chair,
	DirectionsCarFilledSharp,
	Keyboard,
	KeyboardBackspace,
	Laptop,
	Monitor,
	Videocam,
} from '@mui/icons-material';
import { InputField } from '../../components/Input';
import { RoomById } from '../RoomId/RoomById';
import { GoBack } from '../../components/GoToHome';

import Lottie from 'react-lottie';
import animation from '../../assests/91992-walking-body.json';
import SendRoom from './SendRoom/SendRoom';

const Admin = () => {
	const classes = useStyles();

	return (
		<div className={classes.adminContainer}>
			<HeaderElement />
			{/* <BookingElement /> */}
			{/* <SendNewRoom /> */}
			<SendRoom />

			<GoBack />
		</div>
	);
};

export default Admin;

const HeaderElement = () => (
	<div
		style={{
			width: '100%',
			height: '100px',
			backgroundColor: 'rgba(0,0,0,.12)',
			display: 'flex',
			justifyContent: 'space-around',
			alignItems: 'center',
		}}
	>
		<div>
			{/* <img
				style={{ width: '100%', height: '100%' }}
				src="/assests/logo_mediconsult.jpg"
				alt="Pic"
			/> */}
			Logo
		</div>
		<div>Something to do here</div>
	</div>
);

const BookingElement = () => {
	const classes = useStyles();

	const [allRooms, setAllRooms] = useState(false);
	const [newRoom, setNewRoom] = useState(false);
	const [roomById, setRoomById] = useState(false);
	console.log(allRooms.length);
	console.log(allRooms);

	const getRooms = () => {
		checkAllRooms().then((rooms) => setAllRooms(rooms.data));
	};

	const getOneRoomById = (id) => {
		getRoomById(id).then((room) => setRoomById(room.data));
	};
	useEffect(() => {
		getRooms();
	}, []);

	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: animation,
	};
	return (
		<>
			{roomById ? (
				<RoomById oneRoom={roomById} closeEditAndDelete={setRoomById} />
			) : (
				// <div style={{ height: !allRooms ? '80vh' : '80vh' }}>
				<div style={{ height: '100%' }}>
					<h1 style={{ marginLeft: '10%' }}>
						Hello Admin! <small>What do you want to do?</small>
					</h1>
					<div>
						{allRooms ? (
							<Button
								variant={!allRooms ? 'contained' : 'outlined'}
								color={!allRooms ? 'info' : 'error'}
								onClick={() => setAllRooms(!allRooms)}
							>
								Close rooms
							</Button>
						) : (
							<Button
								variant={!allRooms ? 'contained' : 'outlined'}
								color={!allRooms ? 'info' : 'error'}
								onClick={getRooms}
							>
								<span onClick={() => setNewRoom(false)}>See all rooms</span>
							</Button>
						)}

						<Button
							variant={newRoom ? 'outlined' : 'contained'}
							color={newRoom ? 'error' : 'success'}
							onClick={() => setNewRoom(!newRoom)}
						>
							<span onClick={() => setAllRooms(false)}>{newRoom ? 'Close' : 'Create a room'}</span>
						</Button>
					</div>
					{newRoom ? (
						<>
							<div className={classes.newRoom}>
								<NewRoomDialog closeNewRoom={newRoom} />
							</div>
							<div className={classes.buttonBack}>
								<Button
									variant="outlined"
									color="error"
									startIcon={<KeyboardBackspace />}
									onClick={() => setNewRoom(!newRoom)}
								>
									Back
								</Button>
							</div>
						</>
					) : (
						<div
							style={{
								postion: 'relative',
								display: 'flex',
								flexDirection: 'column',
								marginLeft: '10px',
							}}
						>
							<div
								style={{
									display: allRooms === false ? 'none' : 'flex',
									flexDirection: 'column',
									marginTop: '10px',
									borderRadius: '5px',
								}}
							>
								{!allRooms.length ? (
									<div
										style={{
											display: 'flex',
											alignItems: 'center',
											justifyContent: 'space-around',
										}}
									>
										<h1>Loading ...</h1>
										<Lottie options={defaultOptions} width={400} height={400} />
									</div>
								) : (
									<>
										{allRooms.map((room, index) => (
											<div key={index}>
												<h1>{room.city}</h1>
												<div style={{ width: '70vh', height: '100%' }}>
													{room.chairs.map((pair, index) => (
														<div
															key={index}
															style={{
																display: 'flex',
																justifyContent: 'space-between',
																alignItems: 'center',
															}}
														>
															<h3>{index + 1}</h3>
															<span>IsActive: {pair.isActive ? 'Active' : 'Disable'}</span>
															<span>Camera: {pair.camera && <Videocam />}</span>
															<span>Chair: {pair.chair && <Chair />}</span>
															<span>Keyboard: {pair.keyboard && <Keyboard />}</span>
															<span>Laptop: {pair.laptop && <Laptop />}</span>
															<span>Otherscreen: {pair.otherScreens && <Monitor />}</span>
														</div>
													))}
												</div>
											</div>
										))}
									</>
									// <table style={{ borderSpacing: '1px' }}>
									// 	<thead>
									// 		<tr>
									// 			<th className={classes.th}>City</th>
									// 			<th className={classes.th}>#</th>
									// 			{/* <th className={classes.th}>ChairsAmount</th>
									// 			<th className={classes.th}>With 2 monitors</th>
									// 			<th className={classes.th}>With 3 monitors</th> */}
									// 			<th className={classes.th}>Else</th>
									// 		</tr>
									// 	</thead>
									// 	<tbody>
									// 		{allRooms.map((room, index) => (
									// 			<tr key={index}>
									// 				<td className={classes.th}>{room.city}</td>
									// 				<td className={classes.th}>{room.room}</td>
									// 				{/* <td className={classes.th}>{room.chairsAmount}</td>
									// 				<td className={classes.th}>{room.chairType1}</td>
									// 				<td className={classes.th}>{room.chairType2}</td>
									// 				<td className={classes.th}>{room.chairType3}</td> */}
									// 				<td>{room.chairs.length}</td>
									// 				<td>
									// 					{room.chairs.map((chair, index) => (
									// 						<p key={index}>{chair.note}</p>
									// 					))}
									// 				</td>
									// 				<td className={classes.th}>
									// 					<Button
									// 						size="small"
									// 						variant="text"
									// 						startIcon={<ModeEdit style={{ color: 'green' }} />}
									// 						endIcon={<DeleteForever style={{ color: 'red' }} />}
									// 						onClick={() => getOneRoomById(room._id)}
									// 					>
									// 						-
									// 					</Button>
									// 				</td>
									// 			</tr>
									// 		))}
									// 	</tbody>
									// </table>
								)}
							</div>
						</div>
					)}
				</div>
			)}
		</>
	);
};

const NewRoomDialog = () => {
	const classes = useStyles();
	const [values, setValues] = useState({
		adminId: '000000',
		city: 'Turku',
		room: '',
		parking: false,
		chairsAmount: 4,
		chairType1: 1,
		chairType2: 1,
		chairType3: 1,
		date: Date.now(),
	});

	const [isOpen, setIsOpen] = useState(false);
	const [selectedOption, setSelectedOption] = useState('Turku');

	const options = ['Turku', 'Helsinki', 'Oulu', 'Salo', 'Joensuu'];

	const setOption = (value) => {
		setSelectedOption(value);
		setValues({ ...values, city: value });
		setIsOpen(false);
	};
	const sendNewRoom = (values) => {
		addRoom(values)
			.then((response) => console.log(response))
			.catch((err) => console.log(err));
	};

	const sumatoria =
		Number(values.chairType1) + Number(values.chairType2) + Number(values.chairType3);

	return (
		<>
			<div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
				<div
					style={{
						width: '80vh',
						height: '100%',
						backgroundColor: 'rgba(255, 255, 255, 0.09831)',
						paddingTop: '3px ',
						paddingBottom: '10px',
						marginTop: '10px',
						borderRadius: '5px',
						display: 'flex',
						justifyContent: 'space-around',
					}}
				>
					<div className={classes.left}>
						<div>
							<InputField
								name="City"
								value={values.city}
								className={[classes.inputField, classes.input]}
								onChange={(event) => setValues({ ...values, city: event.target.value })}
								required
							/>
						</div>
						<div className={classes.mainDropdown}>
							<div className={classes.dropDownHeader} onClick={() => setIsOpen(!isOpen)}>
								{selectedOption}
							</div>

							{isOpen && (
								<ul className={classes.dropDownList}>
									{options.map((option, index) => (
										<li
											className={classes.listItem}
											key={index}
											onClick={(e) => setOption(e.target.innerText)}
										>
											{option}
										</li>
									))}
								</ul>
							)}
						</div>
					</div>
					<div className={classes.right}>
						<div>
							<InputField
								name="Room"
								value={values.room}
								onChange={(event) => setValues({ ...values, room: event.target.value })}
								required
							/>
						</div>
						<div>
							<h4
								style={{
									display: 'flex',
									alignItems: 'center',
									width: '90px',
									justifyContent: 'space-between',
								}}
							>
								Parking
								<DirectionsCarFilledSharp style={{ color: !values.parking ? 'red' : 'green' }} />
							</h4>
							<label>
								<input
									type="radio"
									name="Parking"
									value={values.parking}
									onChange={(event) => setValues({ ...values, parking: false })}
								/>
								No
							</label>
							<label>
								<input
									type="radio"
									name="Parking"
									value={values.parking}
									onChange={(event) => setValues({ ...values, parking: true })}
								/>
								Yes
							</label>
							{/* <LocalParking /> */}
						</div>

						<div>
							<h2>Total of chairs in the room</h2>

							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder="4"
								name="Amount of chairs"
								onClick={(event) => setValues({ ...values, chairsAmount: event.target.value })}
							/>
							{sumatoria === values.chairsAmount && <h3 style={{ color: 'green' }}>Stop now!</h3>}
							{sumatoria > values.chairsAmount && (
								<div
									style={{
										width: '250px',
										textAlign: 'center',
										border: '1px solid rgba(250,20,23,.8)',
										backgroundColor: 'rgba(0,0,0,0.05)',
										borderRadius: 5,
										marginTop: '3px',
									}}
								>
									<span style={{ color: 'rgba(250,0,23,.8)', fontWeight: 'bolder' }}>
										Check out if you want to rise or decrease the amount
									</span>
								</div>
							)}
						</div>
						<div>
							<h2>With 2 monitors: {values.chairType1}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder="1"
								name="chairType1"
								onClick={(event) => setValues({ ...values, chairType1: event.target.value })}
							/>
						</div>
						<div>
							<h2>With 3 monitors: {values.chairType2}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder="1"
								name="chairType2"
								onClick={(event) => setValues({ ...values, chairType2: event.target.value })}
							/>
						</div>

						<div>
							<h2>With 4 monitors: {values.chairType3}</h2>
							<input
								type="number"
								id="number"
								min={1}
								max={30}
								placeholder="1"
								name="chairType3"
								onClick={(event) => setValues({ ...values, chairType3: event.target.value })}
							/>
						</div>
					</div>
					{/* <h2> Anithing else:</h2> */}
				</div>
				{sumatoria > values.chairsAmount ? (
					<Button variant="contained" disabled>
						<span
							style={{
								color: '#fe5f75',
								textShadow: '0 .48px 5px 1px rgba(255, 0, 0, 0.1)',
								fontWeight: 'bolder',
							}}
						>
							Total of chairs can't be less than the amount of different types ofchairs
						</span>
					</Button>
				) : (
					<div className={classes.buttonNewRoom}>
						<Button variant="contained" onClick={() => sendNewRoom(values)}>
							{sumatoria === values.chairsAmount
								? 'Seems like the room has the same amount of chairs'
								: "Now it's good to go."}
						</Button>
					</div>
				)}
			</div>
		</>
	);
};
