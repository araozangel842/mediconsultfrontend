import {
	Accessible,
	Cancel,
	Create,
	DoNotDisturbAltTwoTone,
	Keyboard,
	Laptop,
	Monitor,
	Videocam,
} from '@mui/icons-material';
import React, { useEffect, useState } from 'react';
import { addRoom } from '../../../api/api';
import { useStyles } from './styles';

const SendNewRoom = () => {
	const classes = useStyles();
	const [values, setValues] = useState();

	const sendRoom = (values) => {
		addRoom(values)
			.then((response) => console.log(response))
			.catch((error) => console.error(error));
	};

	return (
		<div style={{ height: 'fit-content' }}>
			<h1>Welcome admin</h1>
			<h2>Create or edit a room</h2>
			{values && (
				<div
					style={{
						backgroundColor: 'red',
						padding: '4px 10px',
						width: 'fit-content',
						borderRadius: '5px',
						cursor: 'pointer',
					}}
					onClick={() => sendRoom(values)}
				>
					SendNewRoom
				</div>
			)}

			<div className={classes.right}>
				<ChairElement setValues={setValues} />
			</div>
		</div>
	);
};

export default SendNewRoom;

const ChairElement = ({ setValues }) => {
	const roomNumber = new Date().getTime();
	const [selectedOption, setSelectedOption] = useState('Turku');

	const [allChairs, setAllChairs] = useState({
		adminId: localStorage.getItem('userMail'),
		city: '' || selectedOption,
		room: '' || roomNumber,
		chairs: [
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
			{
				userName: '',
				userEmail: '',
				isActive: true,
				otherScreens: true,
				chair: true,
				camera: true,
				laptop: true,
				keyboard: true,
				notes: '',
			},
		],
		parking: false,
	});
	const [isOpen, setIsOpen] = useState(false);

	const options = ['Turku', 'Helsinki', 'Oulu', 'Salo', 'Joensuu'];
	const setOption = (value) => {
		setSelectedOption(value);
		setAllChairs(allChairs, (allChairs.city = value));
		setIsOpen(false);
	};
	const classes = useStyles();

	const sendAllValues = () => {
		console.log(allChairs);
		setValues(allChairs);
	};
	return (
		<div style={{ width: '100%' }}>
			<div className={classes.left}>
				<div className={classes.mainDropdown}>
					<div className={classes.roomName}>
						<h3>Room Name or Number</h3>
						<input
							type="text"
							placeholder="Room name or number"
							onChange={(event) => setAllChairs(allChairs, (allChairs.room = event.target.value))}
						/>
					</div>
					<div className={classes.dropDownHeader} onClick={() => setIsOpen(!isOpen)}>
						{selectedOption}
					</div>

					{isOpen && (
						<div style={{ position: 'relative' }}>
							<ul className={classes.dropDownList}>
								{options.map((option, index) => (
									<li
										className={classes.listItem}
										key={index}
										onClick={(e) => setOption(e.target.innerText)}
									>
										{option}
									</li>
								))}
							</ul>
						</div>
					)}
				</div>
			</div>
			<div
				style={{
					backgroundColor: 'rgba(0,0,0,.152)',
					margin: '0 auto',
					padding: '10px 5px',
					borderBottom: '4px solid rgba(0,0,0,0.45)',
				}}
			>
				In {allChairs.city}
			</div>
			<table style={{ borderSpacing: '1px', width: '94vw' }}>
				<thead>
					<tr>
						<th className={classes.td}>Spot</th>
						<th className={classes.td}>Status</th>
						<th className={classes.td}>
							<Monitor />
						</th>
						<th className={classes.td}>
							<Accessible />
						</th>
						<th className={classes.td}>
							<Videocam />
						</th>
						<th className={classes.td}>
							<Laptop />
						</th>
						<th className={classes.td}>
							<Keyboard />
						</th>
						<th className={classes.tdNote}>
							Notes
							<Create />
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td className={classes.th}>FC1</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[0].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].isActive = !allChairs.chairs[0].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[0].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							{/* <div style={{ display: 'flex', width: 'fit-content', backgroundColor: 'red' }}> */}
							<input
								type="checkbox"
								value={allChairs.chairs[0].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].otherScreens = !allChairs.chairs[0].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[0].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[0].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].chair = !allChairs.chairs[0].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[0].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[0].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].camera = !allChairs.chairs[0].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[0].camera && <Cancel color="error" />}
							</span>
						</td>

						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[0].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].laptop = !allChairs.chairs[0].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[0].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[0].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].keyboard = !allChairs.chairs[0].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[0].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[0].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[0].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC2</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].isActive = !allChairs.chairs[1].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[1].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].otherScreens = !allChairs.chairs[1].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[1].otherScreens && <Cancel color="error" />}
							</span>
						</td>

						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].chair = !allChairs.chairs[1].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[1].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].camera = !allChairs.chairs[1].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[1].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].laptop = !allChairs.chairs[1].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[1].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[1].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].keyboard = !allChairs.chairs[1].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[1].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[1].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[1].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC3</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].isActive = !allChairs.chairs[2].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[2].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].otherScreens = !allChairs.chairs[2].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[2].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].chair = !allChairs.chairs[2].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[2].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].camera = !allChairs.chairs[2].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[2].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].laptop = !allChairs.chairs[2].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[2].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[2].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].keyboard = !allChairs.chairs[2].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[2].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[2].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[2].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC4</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].isActive = !allChairs.chairs[3].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[3].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].otherScreens = !allChairs.chairs[3].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[3].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].chair = !allChairs.chairs[3].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[3].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].camera = !allChairs.chairs[3].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[3].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].laptop = !allChairs.chairs[3].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[3].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[3].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].keyboard = !allChairs.chairs[3].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[3].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[3].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[3].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC5</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].isActive = !allChairs.chairs[4].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[4].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].otherScreens = !allChairs.chairs[4].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[4].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].chair = !allChairs.chairs[4].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[4].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].camera = !allChairs.chairs[4].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[4].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].laptop = !allChairs.chairs[4].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[4].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[4].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].keyboard = !allChairs.chairs[4].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[4].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[4].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[4].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC6</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].isActive = !allChairs.chairs[5].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[5].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].otherScreens = !allChairs.chairs[5].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[5].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].chair = !allChairs.chairs[5].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[5].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].camera = !allChairs.chairs[5].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[5].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].laptop = !allChairs.chairs[5].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[5].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[5].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].keyboard = !allChairs.chairs[5].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[5].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[5].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[5].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC7</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].isActive = !allChairs.chairs[6].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[6].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].otherScreens = !allChairs.chairs[6].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[6].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].chair = !allChairs.chairs[6].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[6].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].camera = !allChairs.chairs[6].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[6].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].laptop = !allChairs.chairs[6].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[6].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[6].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].keyboard = !allChairs.chairs[6].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[6].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[6].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[6].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC8</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].isActive = !allChairs.chairs[7].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[7].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].otherScreens = !allChairs.chairs[7].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[7].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].chair = !allChairs.chairs[7].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[7].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].camera = !allChairs.chairs[7].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[7].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].laptop = !allChairs.chairs[7].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[7].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[7].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].keyboard = !allChairs.chairs[7].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[7].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[7].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[7].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC9</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].isActive = !allChairs.chairs[8].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[8].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].otherScreens = !allChairs.chairs[8].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[8].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].chair = !allChairs.chairs[8].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[8].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].camera = !allChairs.chairs[8].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[8].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].laptop = !allChairs.chairs[8].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[8].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[8].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].keyboard = !allChairs.chairs[8].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[8].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[8].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[8].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC10</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].isActive = !allChairs.chairs[9].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[9].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].otherScreens = !allChairs.chairs[9].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[9].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].chair = !allChairs.chairs[9].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[9].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].camera = !allChairs.chairs[9].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[9].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].laptop = !allChairs.chairs[9].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[9].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[9].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].keyboard = !allChairs.chairs[9].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[9].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[9].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[9].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC11</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].isActive = !allChairs.chairs[10].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[10].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].otherScreens = !allChairs.chairs[10].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[10].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].chair = !allChairs.chairs[10].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[10].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].camera = !allChairs.chairs[10].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[10].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].laptop = !allChairs.chairs[10].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[10].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[10].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].keyboard = !allChairs.chairs[10].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[10].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[10].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[10].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
					<tr>
						<td className={classes.th}>FC12</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].isActive}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].isActive = !allChairs.chairs[11].isActive),
									})
								}
							/>
							<span className={classes.thActive}>
								{!allChairs.chairs[11].isActive && <DoNotDisturbAltTwoTone color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].otherScreens}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].otherScreens = !allChairs.chairs[11].otherScreens),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[11].otherScreens && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].chair}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].chair = !allChairs.chairs[11].chair),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[11].chair && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].camera}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].camera = !allChairs.chairs[11].camera),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[11].camera && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].laptop}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].laptop = !allChairs.chairs[11].laptop),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[11].laptop && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<input
								type="checkbox"
								value={allChairs.chairs[11].keyboard}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].keyboard = !allChairs.chairs[11].keyboard),
									})
								}
							/>
							<span className={classes.thActiveOther}>
								{!allChairs.chairs[11].keyboard && <Cancel color="error" />}
							</span>
						</td>
						<td className={classes.th}>
							<textarea
								style={{ width: '96%' }}
								type="text"
								value={allChairs.chairs[11].notes}
								onChange={(event) =>
									setAllChairs({
										...allChairs,
										...(allChairs.chairs[11].notes = event.target.value),
									})
								}
							/>
						</td>
					</tr>
				</tbody>
			</table>
			<div onClick={() => sendAllValues()} className={classes.buttonSave}>
				Save
			</div>
		</div>
	);
};
