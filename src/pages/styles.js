import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	//Login styles
	login: {
		width: '400px',
		height: 'fit-content',
		display: 'flex',
		flexDirection: 'column',
		margin: '0 auto',
		marginTop: '40px',
		textAlign: 'center',
		color: 'rgb(0,91,126)',
	},
	adminContainer: {
		width: '96%',
		height: '100%',
		margin: '0 auto',
	},
	container: {
		width: '100%',
		height: '100%',
		display: 'flex',
	},
	//Header
	header: {
		width: '100%',
	},
	th: {
		border: '1px solid #999',
		padding: 'o.5rem',
		textAlign: 'center',
	},
	//newRoomDialog
	newRoom: {
		position: 'relative',
		marginTop: '10px',
		backgroundColor: 'rgba(255, 255, 255, 0.14)',
		display: 'flex',
		justifyContent: 'center',
		flexDirection: 'column',
		paddingTop: '10px',
		paddingBottom: '10px',
		marginBottom: '.8rem',
		borderRadius: '5px',
		boxShadow: '0 .48px 5px 1px rgba(0, 0, 0, 0.1)',
	},
	buttonBack: {
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		cursor: 'pointer',
	},
	inputField: {
		// color: 'rgb(0,91,126)',
		fontFamily: 'Roboto',
		display: 'flex',
	},
	input: {
		border: 'none',
		outline: 'none',
	},
	//Dropdown
	mainDropdown: {
		width: '10.5rem',
		margin: '0 auto',
		postion: 'relative',
	},
	dropDownHeader: {
		marginBottom: '.8rem',
		marginTop: '.8rem',
		padding: '.4em 2em .4em 1em',
		boxShadow: '2px 5px 13px rgba(0, 0, 0, 0.16)',
		fontWeight: 'bold',
		fontSize: '1.3rem',
		color: '#3faffa',
		backgroundColor: '#ffffff',
		borderRadius: '5px',
		textAlign: 'center',
		cursor: 'pointer',
		postion: 'absolute',
		zIndex: 1000000,
		'&:hover': {
			backgroundColor: 'rgba(0,0,0,0.2)',
			color: 'white',
		},
	},
	dropDownList: {
		padding: 0,
		margin: 0,
		boxShadow: '2px 5px 13px rgba(0, 0, 0, 0.16)',
		backgroundColor: '#fff',
		border: '1px solid #e5e5e5',
		boxSizing: 'border-box',
		color: '#3faffa',
		fontSize: '1.3rem',
		fontWeight: 'bold',
		borderRadius: '5px',
	},
	listItem: {
		listStyle: 'none',
		marginBottom: '.8rem',
		margin: 'auto',
		textAlign: 'center',
		cursor: 'pointer',
		'&:hover': {
			backgroundColor: '#e5e5e5',
			fontWeight: 'bolder',
			border: '1px solid #e5e5e5',
			color: 'rgba(0,0,0,0.68)',
		},
	},
	image: {
		width: '100px',
		height: '100px',
	},
	buttonNewRoom: {
		width: '100%',
		display: 'flex',
		justifyContent: 'center',
		cursor: 'pointer',
	},
});
