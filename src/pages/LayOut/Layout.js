import React from 'react';
// import styles from './styles';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	layout: {
		padding: '.58rem 1rem',
		margin: 0,
		backgroundColor: 'rgba(255, 255, 255, 0.1)',
	},
});

export const Layout = ({ children }) => {
	const clases = useStyles();
	return <div className={[clases.layout]}>{children}</div>;
};
