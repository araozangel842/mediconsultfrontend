import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	main: {
		width: '100%',
		height: '100%',
		backgroundColor: 'transparent',
		padding: '10px',
		borderRadius: '5px',
	},
});
